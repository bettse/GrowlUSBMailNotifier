//
//  GrowlSamplePrefs.m
//  Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import "GrowlUMNPrefs.h"

@implementation GrowlUMNPrefs

- (NSString *) mainNibName {
	return @"GrowlUMNPrefs";
}

- (void) awakeFromNib {
	[self setDisplayPlugins:[[[GrowlPluginController sharedController] displayPlugins] valueForKey:GrowlPluginInfoKeyName]];
    if(colors == nil){
        colors = [NSArray arrayWithObjects:@"Off", @"Blue", @"Red", @"Green", @"Lt. Blue", @"Fushia", @"Yellow", @"White", nil];
    }
}

- (void) mainViewDidLoad {
}

- (void) didSelect {
	SYNCHRONIZE_GROWL_PREFS();
}

- (NSString *) alternateDisplay {
	NSString* value = nil;
	READ_GROWL_PREF_VALUE(UMN_ALTDISPLAY_PREF, UMNPrefDomain, NSString *, &value);
	if (value && [value isKindOfClass:[NSString class]]){
		NSLog(@"%s returning %@",__PRETTY_FUNCTION__, value);
		return value;
	}else{
		return nil;
	}
}

- (void) setAlternateDisplay:(NSString *)value {
	NSLog(@"%s setting %@",__PRETTY_FUNCTION__, value);
	WRITE_GROWL_PREF_VALUE(UMN_ALTDISPLAY_PREF, value, UMNPrefDomain);
	UPDATE_GROWL_PREFS();
}

- (NSString *) idleColor {
	NSNumber* value = nil;
	READ_GROWL_PREF_VALUE(UMN_IDLECOLOR_PREF, UMNPrefDomain, NSNumber *, &value);
	if (value && [value isKindOfClass:[NSNumber class]]){
        NSLog(@"%s returning %@",__PRETTY_FUNCTION__, [colors objectAtIndex:[value integerValue]]);
		return [colors objectAtIndex:[value integerValue]];
	}else{
		return @"Off";
	}
}

- (void) setIdleColor:(NSString *)value {
    //look up int from array
    int color_code = 0x00;
    color_code = (int)[colors indexOfObject:value];
	NSLog(@"%s setting %@",__PRETTY_FUNCTION__, value);
	WRITE_GROWL_PREF_INT(UMN_IDLECOLOR_PREF, color_code, UMNPrefDomain);
	UPDATE_GROWL_PREFS();
}


- (NSArray *) displayPlugins {
    return plugins;
}

- (NSArray *) colors {
    if(colors == nil){
        colors = [NSArray arrayWithObjects:@"Off", @"Blue", @"Red", @"Green", @"Lt. Blue", @"Fushia", @"Yellow", @"White", nil];
    }
    return colors;
}

- (void) setDisplayPlugins:(NSArray *)thePlugins {
    if (thePlugins != plugins) {
        [plugins release];
        plugins = [thePlugins retain];
    }    
}


@end
