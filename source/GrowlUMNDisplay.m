//
//  GrowlSampleDisplay.h
//  Growl Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import "GrowlUMNDisplay.h"


@implementation GrowlUMNDisplay

- (id) init {
	self = [super init];
    bufferSize = 5;
    inputBuffer = malloc(bufferSize);
    outputBuffer = malloc(bufferSize);
    memset(outputBuffer, 0, bufferSize);
    [self initAppDict];

    READ_GROWL_PREF_INT(UMN_IDLECOLOR_PREF, UMNPrefDomain, &idleColor);
    outputBuffer[0] = idleColor;
    sendRet = IOHIDDeviceSetReport(deviceRef, kIOHIDReportTypeOutput, 0, (uint8_t *)outputBuffer, bufferSize);


	/*
	 *
	 * This code borrows heavily (pretty much completely) from http://osdir.com/ml/usb/2009-09/msg00019.html
	 *
	 */

    //1) Setup your manager and schedule it with the main run loop:

    IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault,
                                                    kIOHIDOptionsTypeNone);
    IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(),
                                    kCFRunLoopDefaultMode);
    ret = IOHIDManagerOpen(managerRef, 0L);

    //2) Get your device:


    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLong:productId] forKey:[NSString
                                                                stringWithCString:kIOHIDProductIDKey encoding:NSUTF8StringEncoding]];
    [dict setObject:[NSNumber numberWithLong:vendorId] forKey:[NSString
                                                               stringWithCString:kIOHIDVendorIDKey encoding:NSUTF8StringEncoding]];
    IOHIDManagerSetDeviceMatching(managerRef, (CFMutableDictionaryRef)dict); NSSet *allDevices = [((NSSet *)IOHIDManagerCopyDevices(managerRef)) autorelease];
    NSArray *deviceRefs = [allDevices allObjects];
    deviceRef = ([deviceRefs count]) ? (IOHIDDeviceRef)[deviceRefs objectAtIndex:0] : nil;

    //3) Setup your buffers (I'm making the assumption the input and output buffer sizes are 64 bytes):


    IOHIDDeviceRegisterInputReportCallback(deviceRef, (uint8_t *)inputBuffer, bufferSize, MyInputCallback, NULL);

	return self;
}

- (void) dealloc {
	[preferencePane release];
	[super dealloc];
}

- (NSPreferencePane *) preferencePane {
	if (!preferencePane)
		preferencePane = [[GrowlUMNPrefs alloc] initWithBundle:[NSBundle bundleForClass:[GrowlUMNPrefs class]]];
	return preferencePane;
}

- (void) configureBridge:(GrowlNotificationDisplayBridge *)theBridge {
	//Save bridge in case we want to forward notification to a different plugin
	lastBridge = theBridge;
}


#pragma mark -
#pragma mark GrowlPositionController Methods
#pragma mark -

- (BOOL)requiresPositioning {
	return NO;
}

#pragma mark -
#pragma mark displayNotification
#pragma	mark -

- (void) displayNotification:(GrowlApplicationNotification *)notification {
    NSString * appName = [notification applicationName];
    NSArray *pattern = [appDict objectForKey:appName];
	NSString * AlternateStyle = nil;

	//If we don't have a pattern (For example, Firefox isn't currently defined in appDict), then use the Growl display selected in the preference panel.
	if(pattern == nil){
		READ_GROWL_PREF_VALUE(UMN_ALTDISPLAY_PREF, UMNPrefDomain, NSString *, &AlternateStyle);
		if (AlternateStyle && [AlternateStyle isKindOfClass:[NSString class]]){
			GrowlDisplayPlugin *plugin = (GrowlDisplayPlugin *)[[GrowlPluginController sharedController] displayPluginInstanceWithName:AlternateStyle
																																author:nil
																															   version:nil
																																  type:nil];
			if (lastBridge) {
				[plugin configureBridge:lastBridge];
			}
			[plugin displayNotification:notification];
			NSLog(@"%s No pattern specified for appName %@, using AlternateStyle %@", __PRETTY_FUNCTION__, appName, AlternateStyle);
		}
	}else{
		//For known patterns, iterate though and display the colors defined.  They are arrays and can be arbitratily long, but each color will be given a half second.
		for (NSNumber *n in [pattern objectEnumerator]){
			outputBuffer[0] = [n integerValue];
			sendRet = IOHIDDeviceSetReport(deviceRef, kIOHIDReportTypeOutput, 0, (uint8_t *)outputBuffer, bufferSize);
			[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
		}
	}

    //Set back to the idle color.  The idea being to at some point allow a constant color for some notification, then these notifications would only interrupt it briefly.
	//For example, have solid green for unread emails, then blink yellow for a skype message, and return to green.
	READ_GROWL_PREF_INT(UMN_IDLECOLOR_PREF, UMNPrefDomain, &idleColor);
    outputBuffer[0] = idleColor;
    sendRet = IOHIDDeviceSetReport(deviceRef, kIOHIDReportTypeOutput, 0, (uint8_t *)outputBuffer, bufferSize);
}


- (void) initAppDict{
	//Create some default patterns.  These are Arrays of numbers, which coorespond to light colors and each color will be given .5 seconds before switch to the next.
    NSArray * email = [NSArray arrayWithObjects:[NSNumber numberWithInt:RED], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:RED], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:RED], [NSNumber numberWithInt:OFF], nil];
    NSArray * im = [NSArray arrayWithObjects:[NSNumber numberWithInt:BLUE], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:BLUE], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:BLUE], [NSNumber numberWithInt:OFF], nil];
    NSArray * skype = [NSArray arrayWithObjects:[NSNumber numberWithInt:GREEN], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:GREEN], [NSNumber numberWithInt:OFF], [NSNumber numberWithInt:GREEN], [NSNumber numberWithInt:OFF], nil];
    NSArray * filesharing = [NSArray arrayWithObjects:[NSNumber numberWithInt:PURPLE], [NSNumber numberWithInt:BLUE], [NSNumber numberWithInt: PURPLE], [NSNumber numberWithInt:BLUE], [NSNumber numberWithInt: PURPLE], [NSNumber numberWithInt:BLUE], nil];
    NSArray * allColors = [NSArray arrayWithObjects:[NSNumber numberWithInt:BLUE], [NSNumber numberWithInt:RED], [NSNumber numberWithInt:GREEN], [NSNumber numberWithInt:LTBLUE], [NSNumber numberWithInt:PURPLE], [NSNumber numberWithInt:YELLOW], [NSNumber numberWithInt:WHITE], nil];


	//Assign previously defined patterns to particular application names
    appDict = [[NSDictionary alloc] initWithObjectsAndKeys:
               allColors, @"growlnotify",
               allColors, @"Growl",
               //Email
               email, @"GrowlMail",
               email, @"Thunderbird",
			   email, @"Google+Growl",
               //IM
               im, @"Chax",
               im, @"Adium",
               im, @"Colloquy",
               skype, @"Skype",
               //filesharing
               filesharing, @"Transmission",
               filesharing, @"SABnzbd",
               nil];

}

void MyInputCallback(void *context, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    //NSLog(@"MyInputCallback called");
    // process device response buffer (report) here
}


@end
