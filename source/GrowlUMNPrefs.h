//
//  GrowlSamplePrefs.h
//  Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import <PreferencePanes/PreferencePanes.h>
#import "GrowlDefinesInternal.h"
#import <GrowlPluginController.h>


#define UMNPrefDomain				@"org.ericbetts.GrowlUSBMailNotifier"
#define UMN_ALTDISPLAY_PREF         @"alternateDisplay"
#define UMN_IDLECOLOR_PREF          @"idleColor"


@interface GrowlUMNPrefs : NSPreferencePane {
	NSArray     *plugins;
    NSArray     *colors;
}

- (NSString *) alternateDisplay;
- (void) setAlternateDisplay:(NSString *)value;
- (NSString *) idleColor;
- (void) setIdleColor:(NSString *)value;
- (NSArray *) displayPlugins;
- (void) setDisplayPlugins:(NSArray *)thePlugins;
- (NSArray *) colors;

@end
