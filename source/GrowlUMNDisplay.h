//
//  GrowlSampleDisplay.h
//  Growl Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <GrowlDisplayPlugin.h>
#import "GrowlUMNPrefs.h"
#import "GrowlApplicationNotification.h"
#import <GrowlDefinesInternal.h>
#import <GrowlDefines.h>
#import <IOKit/hid/IOHIDLib.h>
#include <stdlib.h>
#import <GrowlPluginController.h>


#define OFF 0x00
#define BLUE 0x01
#define RED 0x02
#define GREEN 0x03
#define LTBLUE 0x04
#define PURPLE 0x05
#define YELLOW 0x06
#define WHITE 0x07

@class GrowlApplicationNotification;

const long productId = 0x1320;
const long vendorId = 0x1294;

@interface GrowlUMNDisplay : GrowlDisplayPlugin {
    IOHIDDeviceRef deviceRef;
    IOReturn sendRet;
    IOReturn ret;
    size_t bufferSize;
    char *inputBuffer;
    char *outputBuffer;
    NSDictionary *appDict;
    NSInteger idleColor;//The color to set back to after some particular pattern is shown
	GrowlNotificationDisplayBridge* lastBridge;
}
void MyInputCallback(void *context, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength);
- (void) initAppDict;

@end
